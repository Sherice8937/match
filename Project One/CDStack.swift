//
//  CDStack.swift
//  Lecture 06
//
//  Created by Sherice Sutcliffe on 2/20/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    //Our "notepad"
    let context: NSManagedObjectContext!
    
    //The workhorse. This coordinates everything between our objects, contexts, and store(database on hardrive)
    let persistentStoreCoordinator: NSPersistentStoreCoordinator!
    
    //We modeled our data in the .xcdatamodeld tools
    //We create entities and attributes etc.
    //This will represent our entire .xcdatamodeld file in code
    let objModel: NSManagedObjectModel!
    
    //SQLite?, Binary?, XML? Reads and writes data in whatever format we choose to the disk
    let store: NSPersistentStore!
    
    init() {
        //Setup our objModel
        let xcDataModelURL = Bundle.main.url(forResource: "MemoryGameCD", withExtension: "momd")
        objModel = NSManagedObjectModel(contentsOf: xcDataModelURL!)
        
        //Use the objModel to setup a Coordinator
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: objModel)
        
        //Use the Coordinator to setup the context
        context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = persistentStoreCoordinator
        
        //Use the Coordinator to setup the Store
        
        // 1. Get the documents directory urls
        let directories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        // 2. Create Directory for our Store
        // sub[0] is our main directory
        let storeURL = directories[0].appendingPathComponent("CDStack_Data")
        
        // 3. Create a dictionary of options
        let options = [NSMigratePersistentStoresAutomaticallyOption: true]
        
        // 4. Setup the Store
        do {
            store = try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: options)
        } catch {
            store = nil
        }
    }
    
    func saveContext() {
        
        if context.hasChanges {
            
            do {
                try context.save()
            } catch {
                print("Context was not saved properly.")
            }
            
        }
    }
}
