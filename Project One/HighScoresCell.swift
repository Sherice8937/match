//
//  HighScoresCell.swift
//  Project One
//
//  Created by Sherice Sutcliffe on 2/20/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit

class HighScoresCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var movesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
