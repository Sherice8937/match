//
//  SavedScores.swift
//  Project One
//
//  Created by Sherice Sutcliffe on 2/9/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    // save user name & score
    
    func saveName(name: String, forKey key: String) {
        
        let data = NSKeyedArchiver.archivedData(withRootObject: name)
        
        self.set(data, forKey: key)
    }
    
    func loadName(forKey key: String) -> String {
        
        let binaryData = self.data(forKey: key) ?? nil
        
        if binaryData == nil {
            let name = "N/A"
            return name
        } else {
            let name = NSKeyedUnarchiver.unarchiveObject(with: binaryData!) as? String
            return name!
        }
    }
    
    func saveScore(score: String, forKey key: String) {
        
        let scoreData = NSKeyedArchiver.archivedData(withRootObject: score)
        
        self.set(scoreData, forKey: key)
    }
    
    func loadScore(forKey key: String) -> String {
        
        let binaryData = self.data(forKey: key) ?? nil
        
        if binaryData == nil {
            let score = ""
            return score
        } else {
            let score = NSKeyedUnarchiver.unarchiveObject(with: binaryData!) as? String
            return score!
        }
    }
}
