//
//  OpeningScreen.swift
//  Project One
//
//  Created by Sherice Sutcliffe on 2/9/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import CoreData

class OpeningScreen: UIViewController {
    
    
    var managedContext: NSManagedObjectContext!
    // reference to appDelegate
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    // entity description
    var entityDescription: NSEntityDescription!
    // Core Data Object
    var highScores: NSManagedObject!

    // UIImageView Outlets
    @IBOutlet var allImageViews: [UIImageView]!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var scoresButton: UIButton!
    
    let mySerialQueue = DispatchQueue(label: "com.fullsail.MDF1", qos: .userInitiated)
    
    //array to hold all images
    var img = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        managedContext = appDelegate.CDStack.context
        
        entityDescription = NSEntityDescription.entity(forEntityName: "HighScores", in: managedContext)
        
        imagesArray()
        
        for _ in 1...200 {
            startAnimating()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let game = segue.destination as! ViewController //GameScreen
        
        game.managedContext = managedContext
        game.highScores = highScores
        game.entityDescription = entityDescription
        
    }
    
    func imagesArray() {
        
        // all Images to be shown in background
        img.append(#imageLiteral(resourceName: "emoticons happy")); img.append(#imageLiteral(resourceName: "food pork thig")); img.append(#imageLiteral(resourceName: "magic cauldron")); img.append(#imageLiteral(resourceName: "food grape")); img.append(#imageLiteral(resourceName: "chess black pawn")); img.append(#imageLiteral(resourceName: "food grape")); img.append(#imageLiteral(resourceName: "casino dice")); img.append(#imageLiteral(resourceName: "chess black pawn")); img.append(#imageLiteral(resourceName: "dressup lips")); img.append(#imageLiteral(resourceName: "elements fire")); img.append(#imageLiteral(resourceName: "drawing colors")); img.append(#imageLiteral(resourceName: "dressup lips")); img.append(#imageLiteral(resourceName: "casino dice")); img.append(#imageLiteral(resourceName: "magic bulb flask")); img.append(#imageLiteral(resourceName: "drawing colors")); img.append(#imageLiteral(resourceName: "emoticons happy")); img.append(#imageLiteral(resourceName: "magic cauldron")); img.append(#imageLiteral(resourceName: "magic bulb flask")); img.append(#imageLiteral(resourceName: "elements fire")); img.append(#imageLiteral(resourceName: "food pork thig")); img.append(#imageLiteral(resourceName: "Card Back")); img.append(#imageLiteral(resourceName: "military pistol")); img.append(#imageLiteral(resourceName: "military machine gun")); img.append(#imageLiteral(resourceName: "misc camera")); img.append(#imageLiteral(resourceName: "misc game missions")); img.append(#imageLiteral(resourceName: "monetary gold coins"))
        
    }
    
    func startAnimating() {
        
        let randomView = Int(arc4random_uniform(50))
        let randomImg = Int(arc4random_uniform(26))
        
        mySerialQueue.async {
            // delay
            self.LongRunningFunction()
            
            DispatchQueue.main.async {
                self.allImageViews[randomView].image = self.img[randomImg]
            }
        }
    }
    
    // method to block the calling thread for a specified time
    func LongRunningFunction() {
        
        // block thread for 1 second
        Thread.sleep(forTimeInterval: 1)
    }
}
