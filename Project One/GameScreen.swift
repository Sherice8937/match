//
//  ViewController.swift
//  Project One
//
//  Created by Sherice Sutcliffe on 2/5/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    var entityDescription: NSEntityDescription!
    // Core Data Object
    var highScores: NSManagedObject!
    
    var myScores = [(name: String, moves: String, time: String, timeStamp: String)]()
    
    // UIImageView Outlets
    @IBOutlet var allImageViews: [UIImageView]!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    //array of images for iPhone sizes
    var mainImages = [UIImage]()
    // array of images for iPad sizes
    var extraImages = [UIImage]()
    
    // used to track which view is selected
    var currentTag = 0
    // used to track how many cards have been flipped
    var cardsFlipped = 0
    // used to track how many matches have been made
    var matches = 0
    // used when comparing cards, to see if they match
    var previousViewImage: UIImage!
    var firstViewTag = 0
    var secondViewTag = 0
    
    // keep track of number of moves user has made
    var numOfMoves = 0
    
    // date to be saved with High Scores
    let date = NSDate()
    let calendar = NSCalendar.current
    
    var timer: Timer!
    // used to keep track of time
    var seconds = 0
    var minutes = 0
    
    //used to determine if user is on iPad or not
    var iPadMode = false
    
    // tuple for saving user scores
    var highScore = (name: "", score: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mainImagesArray()
        extraImagesArray()
        
        newGame()
        showMatches()
        
        addTapGestures()
        
        loadCoreData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapGestures() {
        
        for card in allImageViews {
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector (ViewController.tapGestureFired(_:)))
            card.addGestureRecognizer(tapGesture)
        }
        
    }
    
    // show all matches for 5 seconds
    func showMatches() {
        
        for card in allImageViews {
            
            // call method to update ImageViews with correct images
            switchCurrentTag(currentView: card)
            
            currentTag += 1
            
            // disable cards so they cant be clicked
            card.isUserInteractionEnabled = false
            
        }
        
        let delay = DispatchTime.now() + 5 // 5 seconds
        DispatchQueue.main.asyncAfter(deadline: delay) {
            
            for card in self.allImageViews {
                card.image = #imageLiteral(resourceName: "Card Back")
                
                // allow the user to click cards again
                card.isUserInteractionEnabled = true
            }
            self.startTimerCount()
        }
        
        seconds = 0
        minutes = 0
        currentTag = 0
        numOfMoves = 0
        iPadMode = false
        
    }
    
    func tapGestureFired(_ sender: UITapGestureRecognizer) {
        
        var currentView: UIImageView!
        currentView = sender.view as! UIImageView!
        currentTag = currentView.tag
        
        // everytime the user clicks a card, move count goes up
        numOfMoves += 1
        
        // check for how many cards are flipped over
        cardsFlipped += 1
        
        switch cardsFlipped {
        case 1:
            // show other side of card
            switchCurrentTag(currentView: currentView)
            firstViewTag = currentView.tag
            // save image to be compared to second image
            previousViewImage = currentView.image
            // disable card so the user cannot click it again
            allImageViews[firstViewTag].isUserInteractionEnabled = false
            
        case 2:
            switchCurrentTag(currentView: currentView)
            secondViewTag = currentView.tag
            
            // check if both cards are the same
            if currentView.image == previousViewImage {
                
                // delay when cards will be removed
                let delay = DispatchTime.now() + 0.5 // desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: delay) {
                    self.allImageViews[self.firstViewTag].image = nil
                    self.allImageViews[self.secondViewTag].image = nil
                    // disable cards so user cannot click them
                    self.allImageViews[self.firstViewTag].isUserInteractionEnabled = false
                    self.allImageViews[self.secondViewTag].isUserInteractionEnabled = false
                    self.cardsFlipped = 0
                    self.matches += 1
                    
                    if self.iPadMode == true {
                        
                        // check if there are any cards left
                        if self.matches == 15 {
                            
                            self.timer.invalidate()
                            self.timer = nil
                            self.gameCompleteAlert()
                        }
                        
                    } else {
                        
                        // check if there are any cards left
                        if self.matches == 10 {
                            
                            self.timer.invalidate()
                            self.timer = nil
                            self.gameCompleteAlert()
                        }
                        
                    }
                    
                }
            } else {
                // delay when cards will be flipped back over
                // so user can see the cards that do not match
                let delay = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: delay) {
                    self.allImageViews[self.firstViewTag].image = #imageLiteral(resourceName: "Card Back")
                    self.allImageViews[self.secondViewTag].image = #imageLiteral(resourceName: "Card Back")
                    // allow user to now click this card again
                    self.allImageViews[self.firstViewTag].isUserInteractionEnabled = true
                    //self.allCards[self.secondViewTag]
                    self.cardsFlipped = 0
                }
            }
            
        default:
            print("An error occured")
        }
    }
    
    func mainImagesArray() {
        
        // main Images
        // for iPhone screens
        mainImages.append(#imageLiteral(resourceName: "emoticons happy")); mainImages.append(#imageLiteral(resourceName: "food pork thig")); mainImages.append(#imageLiteral(resourceName: "magic cauldron")); mainImages.append(#imageLiteral(resourceName: "food grape")); mainImages.append(#imageLiteral(resourceName: "chess black pawn")); mainImages.append(#imageLiteral(resourceName: "food grape")); mainImages.append(#imageLiteral(resourceName: "casino dice")); mainImages.append(#imageLiteral(resourceName: "chess black pawn")); mainImages.append(#imageLiteral(resourceName: "dressup lips")); mainImages.append(#imageLiteral(resourceName: "elements fire")); mainImages.append(#imageLiteral(resourceName: "drawing colors")); mainImages.append(#imageLiteral(resourceName: "dressup lips")); mainImages.append(#imageLiteral(resourceName: "casino dice")); mainImages.append(#imageLiteral(resourceName: "magic bulb flask")); mainImages.append(#imageLiteral(resourceName: "drawing colors")); mainImages.append(#imageLiteral(resourceName: "emoticons happy")); mainImages.append(#imageLiteral(resourceName: "magic cauldron")); mainImages.append(#imageLiteral(resourceName: "magic bulb flask")); mainImages.append(#imageLiteral(resourceName: "elements fire")); mainImages.append(#imageLiteral(resourceName: "food pork thig"))
    }
    
    func extraImagesArray() {
        
        // images to be used for iPad screens
        extraImages.append(#imageLiteral(resourceName: "military machine gun")); extraImages.append(#imageLiteral(resourceName: "military machine gun")); extraImages.append(#imageLiteral(resourceName: "military pistol")); extraImages.append(#imageLiteral(resourceName: "military pistol")); extraImages.append(#imageLiteral(resourceName: "misc camera")); extraImages.append(#imageLiteral(resourceName: "misc game missions")); extraImages.append(#imageLiteral(resourceName: "misc game missions")); extraImages.append(#imageLiteral(resourceName: "misc camera")); extraImages.append(#imageLiteral(resourceName: "monetary gold coins")); extraImages.append(#imageLiteral(resourceName: "monetary gold coins"))
    }
    
    // updates current UIImageView Image
    func switchCurrentTag(currentView: UIImageView) {
        
        switch currentTag {
        case 0...19:
            // set current UIImageView to new image
            currentView.image = mainImages[currentTag]
            
        case 20...29:
            // set current UIImageView to new image
            // for iPad
            let newTag = currentTag - 20
            iPadMode = true
            currentView.image = extraImages[newTag]
            
        default:
            print("There was an error")
        }
    }
    
    func startTimerCount() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (ViewController.updateTime), userInfo: nil, repeats: true)
        }
    }
    
    func updateTime() {
        
        seconds += 1
        
        // make sure time is shown in double digits (00:00)
        if seconds <= 9 {
            timerLabel.text = "0\(minutes):0\(seconds)"
        } else {
            timerLabel.text = "0\(minutes):\(seconds)"
        }
        
        // restart seconds after one minute has passed and count it as a minute
        if seconds == 60 {
            seconds = 0
            minutes += 1
            
            // make sure time is shown in double digits (00:00)
            if minutes <= 9 {
                timerLabel.text = "0\(minutes):0\(seconds)"
            } else {
                timerLabel.text = "\(minutes):0\(seconds)"
            }
        }
    }
    
    // display an alert message to the user when
    func gameCompleteAlert() {
        
        let alert = UIAlertController(title: "Congratulations, You Won!", message: "Your final time was: \(timerLabel.text!)", preferredStyle: .alert)
        
        // set up actions
        
        // save users score
        let save = UIAlertAction(title: "Save Score", style: .default, handler: {
            (action) -> Void in
            
            //show dialog asking user to input their name
            self.saveUserInfo()
            
            
        })
        
        // start a New Game
        let new = UIAlertAction(title: "New Game", style: .default, handler: {
            (action) -> Void in
            
            self.newGame()
            
        })
        
        // quit the game
        let quit = UIAlertAction(title: "Quit", style: .default, handler: {
            (action) -> Void in
            
            // go back to opening screen
            self.dismiss(animated: true, completion: nil)
        })
        
        // round corners
        alert.view.layer.cornerRadius = 25
        
        // add alert actions/buttons to the alert and display it
        alert.addAction(save)
        alert.addAction(new)
        alert.addAction(quit)
        present(alert, animated: true, completion: nil)
        
    }
    
    // shuffle cards & clear timer
    // starts a new game for the user
    func newGame() {
        
        timerLabel.text = "00:00"
        numOfMoves = 0
        
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        
        matches = 0
        
        // shuffle mainImagesArray
        for index in ((0 + 1)...mainImages.count - 1).reversed() {
            
            // Random int from 0 to index-1
            let i = Int(arc4random_uniform(UInt32(index - 1)))
            
            // swap two array elements
            swap(&mainImages[index], &mainImages[i])
            
        }
        
        for index in ((0 + 1)...extraImages.count - 1).reversed() {
            
            // Random int from 0 to index-1
            let i = Int(arc4random_uniform(UInt32(index - 1)))
            
            // swap two array elements
            swap(&extraImages[index], &extraImages[i])
            
        }
        
        seconds = 0
        minutes = 0
        currentTag = 0
        showMatches()
        
    }
    
    func saveUserInfo() {
        
        // get the timeStamp that the user completed their game
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        let month = calendar.component(.month, from: date as Date)
        let year = calendar.component(.year, from: date as Date)
        let day = calendar.component(.day, from: date as Date)
        
        // set the times into one string
        let userTimeStamp = "\(month)/\(day)/\(year). \(hour):\(minutes)"
        
        let saveAlert =  UIAlertController(title: "Save Your Score", message: "Please type your name", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
            (action) -> Void in
            
            // start a new game by default
            self.newGame()
            
        })
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            (action) -> Void in
            
            // field for user to save their name
            let userInitials = (saveAlert.textFields?[0].text)!
            
            self.myScores.append((name: userInitials, moves: self.numOfMoves.description, time: self.timerLabel.text!, timeStamp: userTimeStamp))
            
            self.saveCoreData()
            
            // restart the game
            self.newGame()
            
        })
        
        saveAlert.addTextField { (textField: UITextField) in
            
            textField.keyboardType = .alphabet
        }
        
        saveAlert.addAction(cancelAction)
        saveAlert.addAction(saveAction)
        present(saveAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func restartButtonClicked(_ sender: UIButton) {
        newGame()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let savedScores = segue.destination as! HighScoresTVC
        
        if highScores.value(forKey: "name") != nil {
            savedScores.allSavedScores.append((name: highScores.value(forKey: "name") as! String, time: highScores.value(forKey: "time") as! String, timeStamp: highScores.value(forKey: "timeStamp") as! String, moves: highScores.value(forKey: "moves") as! String))
        }
    }
    
    @IBAction func unwindFromHighScores(segue: UIStoryboardSegue, sender: Any) {
        // start a new game when coming back from High Scores
        newGame()
        
    }
    
    func saveCoreData() {
        
        for s in myScores {
            
            // save user's name
            highScores.setValue(s.name, forKey: "name")
            // save user's moves
            highScores.setValue(s.moves, forKey: "moves")
            // save user's time
            highScores.setValue(s.time, forKey: "time")
            // save user's timeStamp
            highScores.setValue(s.timeStamp, forKey: "timeStamp")
            
        }
    }
    
    func loadCoreData() {
        
        // load saved Core Data
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HighScores")
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            
            if results.count == 0 {
               highScores = NSManagedObject(entity: entityDescription, insertInto: managedContext)
            } else {
                highScores = results[0]
            }
        } catch {
            assertionFailure()
        }
    }
}

